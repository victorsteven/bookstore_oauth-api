package app

import (
	"bookstore_oauth-api/src/http"
	"bookstore_oauth-api/src/repository/db"
	"bookstore_oauth-api/src/repository/rest"
	"bookstore_oauth-api/src/services/access_tokens"
	"github.com/gin-gonic/gin"
)

var (
	router = gin.Default()
)

func StartApp() {
	//session, dbErr := cassandra.GetSession()
	//if dbErr != nil {
	//	panic(dbErr)
	//}
	//session.Close()

	//atService := access_tokens.NewService(db.NewRepository())
	//atHandler := http.NewHandler(atService)

	atHandler := http.NewAccessTokenHandler(access_tokens.NewService(rest.NewRestUsersRepository(), db.NewRepository()))

	router.GET("/oauth/access_token/:access_token_id", atHandler.GetById)
	router.POST("/oauth/access_tokens", atHandler.Create)


	router.Run(":9999")

}