package restclient

import (
	"bookstore_utils-go/rest_errors"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

var (
	ClientRequest ClientInterface = &clientRequest{}
)

type ClientInterface interface {
	Post(url string, body interface{}) (*http.Response, rest_errors.RestErr)
}
type clientRequest struct {}

func (c *clientRequest) Post(url string, body interface{}) (*http.Response, rest_errors.RestErr) {
	fmt.Println("WE ENTERED THIS METHOD")
	jsonBytes, err := json.Marshal(body)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("Cannot unmarshal json", err)
	}
	request, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(jsonBytes))
	if err != nil {
		return nil, rest_errors.NewInternalServerError("error sending request", err)
	}
	client := http.Client{}
	sendValue, err := client.Do(request)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("error sending request", err)
	}
	fmt.Println("this is the thief: ", sendValue)
	return sendValue, nil
}

func Get(url string) (*http.Response, error) {
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	client := http.Client{}
	return client.Do(request)
}
