package restclient

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
)

var (
	postLogin func(url string, body interface{}) (*http.Response, error)
)
//type requestClientMock struct {
//	Email string
//	Password string
//}
type requestClientMock struct {}

func (c *requestClientMock) Post(url string, body interface{}) (*http.Response, error){
	return postLogin(url, body)
}
func TestLoginSuccess(t *testing.T) {
	requestBody := struct {
		Email string
		Password string
	}{
		Email: "man@gmail.com",
		Password: "here",
	}

	ClientRequest = &requestClientMock{}

	//sender := &requestClientMock{}

	//ci := clientRequest{}
	ci := requestClientMock{}

	url := "http://localhost:8888/users/login"

	user, err := ci.Post(url, requestBody)
	fmt.Println("this is the error: ", err)

	bytes, err := ioutil.ReadAll(user.Body)
	fmt.Println("this is the response: ", string(bytes))

	//user, err := repository.LoginUser("man@gmail.com", "here")
	//fmt.Println("this is the error: ", err)
	//fmt.Println("this is the response: ", user)
}