package http

import (
	"bookstore_oauth-api/src/domain/access_token"
	"bookstore_oauth-api/src/services/access_tokens"
	"bookstore_utils-go/rest_errors"
	"github.com/gin-gonic/gin"
	"net/http"
)

type AccessTokenHandler interface {
	GetById (*gin.Context)
	Create(*gin.Context)
}

type accessTokenHandler struct {
	service access_tokens.Service
}

func NewAccessTokenHandler(service access_tokens.Service) AccessTokenHandler {
	return &accessTokenHandler{
		service: service,
	}
}

//func NewHandler(service access_tokens.Service) AccessTokenHandler {
//	return &accessTokenHandler{
//		service: service,
//	}
//}

func (h *accessTokenHandler) GetById(c *gin.Context) {
	accessToken, err := h.service.GetById(c.Param("access_token_id"))
	if err != nil {
		c.JSON(err.Status(), err)
		return
	}
	c.JSON(http.StatusOK, accessToken)
}

func (h *accessTokenHandler) Create(c *gin.Context) {
	var request access_token.AccessTokenRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		restErr := rest_errors.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status(), restErr)
		return
	}

	accessToken, err := h.service.Create(request)
	if err != nil {
		c.JSON(err.Status(), err)
		return
	}
	c.JSON(http.StatusCreated, accessToken)

}