package access_token

import (
	"testing"
	"time"
	"github.com/stretchr/testify/assert"
)

func TestAccessTokenConstants(t *testing.T) {
	assert.EqualValues(t, 24, expirationTime, "expiration time should be 24 hours")
}
func TestGetNewAccessToken(t *testing.T) {
	at := GetNewAccessToken()
	//if at.IsExpired() {
	//	t.Error("brand new access token should not be nil")
	//}
	assert.False(t, at.IsExpired(), "brand new access token should not be nil")
	assert.EqualValues(t, "", at.AcccessToken, "new access token should not have defined access token id")
	assert.True(t, at.UserId == 0)
}

func TestAccessToken_IsExpired(t *testing.T) {
	at := AccessToken{}
	assert.True(t, at.IsExpired(), "empty access token should be expired by default")
	at.Expires = time.Now().UTC().Add(3 * time.Hour).Unix()
	assert.False(t, at.IsExpired(), "access token should not be expiring 3 hours from now")
}