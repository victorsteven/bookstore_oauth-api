package db

import (
	"bookstore_oauth-api/src/clients/cassandra"
	"bookstore_oauth-api/src/domain/access_token"
	"bookstore_utils-go/rest_errors"
	"errors"
	"fmt"
	"github.com/gocql/gocql"
)

const (
	queryGetAccessToken = "SELECT access_token, user_id, client_id, expires FROM access_tokens WHERE access_token=?;"
	queryCreateAccessToken = "INSERT INTO access_tokens(access_token, user_id, client_id, expires) VALUES(?,?,?,?);"
	queryUpdateExpires = "UPDATE access_tokens SET expires=? WHERE access_token=?;"
)

type  DbRepository interface {
	GetById (string) (*access_token.AccessToken, rest_errors.RestErr)
	Create(access_token.AccessToken) rest_errors.RestErr
	UpdateExpirationTime(access_token.AccessToken) rest_errors.RestErr
}
type dbRepository struct {

}
func NewRepository() DbRepository {
 	return &dbRepository{}
}

//func (r *dbRepository) GetById (id string) (*access_token.AccessToken, rest_errors.RestErr){
//	var result access_token.AccessToken
//	if err := cassandra.GetSession().Query(queryGetAccessToken, id).Scan(&result.AccessToken, &result.UserId, &result.ClientId, &result.Expires); err != nil {
//		if err == gocql.ErrNotFound {
//
//			//type restErr struct {
//			//	ErrMessage string        `json:"message"`
//			//	ErrStatus  int           `json:"status"`
//			//	ErrError   string        `json:"error"`
//			//	ErrCauses  []interface{} `json:"causes"`
//			//}
//			fmt.Println("THIS IS THE CORE ERROR: ", rest_errors.NewNotFoundError("no access token found with the give id"))
//
//			return nil, rest_errors.NewNotFoundError("no access token found with the give id")
//		}
//		fmt.Println("this is the current error: ", err)
//		return nil, rest_errors.NewInternalServerError("error when trying to get current id now", errors.New("database error"))
//	}
//	return &result, nil
//}

func (r *dbRepository) GetById(id string) (*access_token.AccessToken, rest_errors.RestErr) {
	var result access_token.AccessToken
	if err := cassandra.GetSession().Query(queryGetAccessToken, id).Scan(
		&result.AccessToken,
		&result.UserId,
		&result.ClientId,
		&result.Expires,
	); err != nil {
		if err == gocql.ErrNotFound {
			fmt.Println("THIS IS THE CORE ERROR: ", rest_errors.NewNotFoundError("no access token found with the give id"))

			return nil, rest_errors.NewNotFoundError("no access token found with given id")
		}
		return nil, rest_errors.NewInternalServerError("error when trying to get current id", errors.New("database error"))
	}
	return &result, nil
}

func (r *dbRepository) Create(at access_token.AccessToken) rest_errors.RestErr {
	fmt.Println("WE ENTERED THE SOUP CREATE")
	if err := cassandra.GetSession().Query(queryCreateAccessToken, at.AccessToken, at.UserId, at.ClientId, at.Expires).Exec(); err != nil {
		return rest_errors.NewInternalServerError("error when trying to save access token", err)
	}
	return nil
}

func (r *dbRepository) UpdateExpirationTime(at access_token.AccessToken) rest_errors.RestErr {
	if err := cassandra.GetSession().Query(queryUpdateExpires, at.Expires, at.AccessToken).Exec(); err != nil {
		return rest_errors.NewInternalServerError("error when trying to update current resource", errors.New("database error"))
	}
	return nil
}


