package rest

import (
	"bookstore_oauth-api/src/clients/restclient"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"testing"
)

var (
	postLogin func(url string, body interface{}) (*http.Response, error)
)

type requestClientMock struct {}

func (c *requestClientMock) Post(url string, body interface{}) (*http.Response, error){
	return postLogin(url, body)
}

func TestLoginSuccess(t *testing.T) {
	postLogin = func(url string, body interface{}) (*http.Response, error) {
		return &http.Response{
			StatusCode:  http.StatusOK,
			Body:       ioutil.NopCloser(strings.NewReader(`{"id": 1, "first_name": "man", "last_name": "gang", "email": "man@gmail.com",  "status": "active"}`)),
		}, nil
	}
	restclient.ClientRequest = &requestClientMock{} //without this, the real Post method is hit

	repository := usersRepository{}
	user, err := repository.LoginUser("man@gmail.com", "password")
	assert.Nil(t, err)
	assert.NotNil(t, user)
	fmt.Println("this is the user response: ", user)
	assert.EqualValues(t, "man@gmail.com", user.Email)
	assert.EqualValues(t, "man", user.FirstName)
	assert.EqualValues(t, "gang", user.LastName)
	assert.EqualValues(t,1 , user.Id)
}

func TestLoginUser(t *testing.T) {

}

//When the user of the api change the datatype of one of the fields, for instance, changing an integer to a string, as in the "Body" status code below
func TestLoginUserInvalidErrorInterface(t *testing.T) {
	postLogin = func(url string, body interface{}) (*http.Response, error) {
		return &http.Response{
			StatusCode:  http.StatusInternalServerError,
			Body:       ioutil.NopCloser(strings.NewReader(`{"message": "invalid user credentials", "status": "404", "error": "not_found"}`)),
		}, nil
	}
	restclient.ClientRequest = &requestClientMock{} //without this, the real Post method is hit

	repository := usersRepository{}
	user, err := repository.LoginUser("steven@gmail.com", "password")
	assert.NotNil(t, err)
	assert.Nil(t, user)
	assert.EqualValues(t, http.StatusInternalServerError, err.Status)
	assert.EqualValues(t, "server_error", err.Error)
	assert.EqualValues(t, "invalid error interface when trying to login user", err.Message)
}

func TestLoginUserInvalidLoginCredentials(t *testing.T) {
	postLogin = func(url string, body interface{}) (*http.Response, error) {
		return &http.Response{
			StatusCode:  http.StatusNotFound,
			Body:       ioutil.NopCloser(strings.NewReader(`{"message": "invalid user credentials", "status": 404, "error": "not_found"}`)),
		}, nil
	}
	restclient.ClientRequest = &requestClientMock{} //without this, the real Post method is hit

	repository := usersRepository{}
	user, err := repository.LoginUser("steven@gmail.com", "password")
	assert.NotNil(t, err)
	assert.Nil(t, user)
	assert.EqualValues(t, http.StatusNotFound, err.Status)
	assert.EqualValues(t, "not_found", err.Error)
	assert.EqualValues(t, "invalid user credentials", err.Message)
}

//we have a response, but the response datatype didnt match the datatype of what we have in our struct. For instance, the id in the response is of type string, while the id datatype of our struct is an int
func TestLoginUserInvalidUserJsonResponse(t *testing.T) {
	postLogin = func(url string, body interface{}) (*http.Response, error) {
		return &http.Response{
			StatusCode:  http.StatusOK,
			Body:       ioutil.NopCloser(strings.NewReader(`{"id": "1", "first_name": "man", "last_name": "gang", "email": "man@gmail.com",  "status": "active"}`)),
		}, nil
	}
	restclient.ClientRequest = &requestClientMock{} //without this, the real Post method is hit

	repository := usersRepository{}
	user, err := repository.LoginUser("man@gmail.com", "password")
	assert.NotNil(t, err)
	assert.Nil(t, user)
	fmt.Println("this is the user err: ", err)
	assert.EqualValues(t, "error when trying to unmarshal users response", err.Message)
	assert.EqualValues(t, http.StatusInternalServerError, err.Status)
	assert.EqualValues(t, "server_error", err.Error)
}

func TestLoginUserInvalidErrorResponse(t *testing.T) {
	invalidCloser, _ := os.Open("-asf3")
	postLogin = func(url string, body interface{}) (*http.Response, error) {
		return &http.Response{
			StatusCode:  http.StatusInternalServerError,
			Body:       invalidCloser,
		}, nil
	}
	restclient.ClientRequest = &requestClientMock{} //without this, the real Post method is hit

	repository := usersRepository{}
	user, err := repository.LoginUser("steven@gmail.com", "password")
	assert.NotNil(t, err)
	assert.Nil(t, user)
	assert.EqualValues(t, http.StatusInternalServerError, err.Status)
	assert.EqualValues(t, "server_error", err.Error)
	assert.EqualValues(t, "error when un marshall response", err.Message)
}

//func TestLoginUserInvalidRestClientError(t *testing.T) {
//	postLogin = func(url string, body interface{}) (*http.Response, error) {
//		return &http.Response{
//			StatusCode:  -1,
//			Body:       ioutil.NopCloser(strings.NewReader(`{"id"": 1}`)),
//		}, nil
//	}
//	restclient.ClientRequest = &requestClientMock{} //without this, the real Post method is hit
//
//	repository := usersRepository{}
//	user, err := repository.LoginUser("steven@gmail.com", "password")
//	assert.NotNil(t, err)
//	assert.Nil(t, user)
//	//assert.EqualValues(t, http.StatusInternalServerError, err.Status)
//	//assert.EqualValues(t, "server_error", err.Error)
//	//assert.EqualValues(t, "error when un marshall response", err.Message)'
//	fmt.Println("this is the error: ", err)
//}
