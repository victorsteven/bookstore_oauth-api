package rest

import (
	"bookstore_oauth-api/src/clients/restclient"
	"bookstore_oauth-api/src/domain/users"
	"bookstore_utils-go/rest_errors"
	"encoding/json"
	"errors"
	"io/ioutil"
)

type RestUsersRepository interface {
	LoginUser(string, string) (*users.User, rest_errors.RestErr)
}
type usersRepository struct {}

//func NewRepository() RestUsersRepository {
//	return &usersRepository{}
//}
func NewRestUsersRepository() RestUsersRepository {
	return &usersRepository{}
}

func (u *usersRepository) LoginUser(email string, password string) (*users.User, rest_errors.RestErr) {
	request := users.UserLoginRequest{
		Email:    email,
		Password: password,
	}
	url := "http://localhost:8888/users/login"
	response, err := restclient.ClientRequest.Post(url, request)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("invalid rest client response when trying to login user", errors.New("restclient error"))
	}
	bytes, errUtil := ioutil.ReadAll(response.Body)
	if errUtil != nil {
		return nil, rest_errors.NewInternalServerError("error when un marshall response", errors.New("invalid body error"))
	}
	if response.StatusCode > 299 {
		apiErr, err := rest_errors.NewRestErrorFromBytes(bytes)
		if err != nil {
			return nil, rest_errors.NewInternalServerError("invalid error interface when trying to login user", err)
		}
		return nil, apiErr
	}
	var user users.User
	if err := json.Unmarshal(bytes, &user); err != nil {
		return nil, rest_errors.NewInternalServerError("error when trying to unmarshal users response", errors.New("restclient error"))
	}
	return &user, nil
}
