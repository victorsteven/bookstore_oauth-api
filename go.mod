module bookstore_oauth-api

go 1.13

require (
	bookstore_utils-go v0.0.0
	github.com/gin-gonic/gin v1.4.0
	github.com/gocql/gocql v0.0.0-20191106222750-ae2f7fc85f32
	github.com/stretchr/testify v1.4.0
)

replace bookstore_utils-go v0.0.0 => ../bookstore_utils-go
